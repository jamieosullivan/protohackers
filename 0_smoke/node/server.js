const net = require("net");

const PORT = 7000;

/*
net.createServer((socket) => {
	console.log("Client connected.");
	socket.pipe(socket);
}).listen(PORT);
*/

net.createServer((socket) => {
	console.log("Client connected.");
	socket.on("data", (input) => {
		console.log(input);
		socket.write(input);
	});
}).listen(PORT);


